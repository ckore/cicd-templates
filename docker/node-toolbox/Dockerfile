# syntax=docker/dockerfile:1
# ---------------------------------------------------------------
# CI image
# - Used for Gitlab (see .gitlab-ci.yml)
# - MUST be updated whenever the CI installation requirements (i.e., packages/versions) change
# ---------------------------------------------------------------
# ARGS are declared before any FROM so that they are globally available
# Reference: https://github.com/docker/for-mac/issues/2155#issuecomment-372639972
ARG authors
ARG node_ver
ARG npm_ver
ARG sls_ver
ARG base_img

# ---------------------------------------------------------------
# Base image
# - Configure cache folders for npm and cypress
# - Install npm and cypress
# ---------------------------------------------------------------
FROM "${base_img}" as base

LABEL authors="${authors}"
LABEL node_version="${node_ver}"
LABEL base_image="${base_img}"
# LABEL cypress_version="${cypress_ver}"
# LABEL npm_version="${npm_ver}"

# Env
ENV NPM_VERSION="${npm_ver}"
ENV SLS_VERSION="${sls_ver}"
# ENV CYPRESS_VERSION="${cypress_ver}"

RUN npm install -g \
  serverless \
  node-gyp \
  node-gyp-build

RUN apt-get update && \
  apt-get install -y --no-install-recommends \
    bash \
    curl \
    jq \
    zip \
    unzip \
    git \
    python \
    ca-certificates \
    make && \
  apt-get purge --auto-remove -y && \
  rm -rf /var/lib/apt/lists/*

RUN curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" && \
  unzip awscliv2.zip && \
  ./aws/install