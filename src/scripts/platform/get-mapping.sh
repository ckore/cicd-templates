#!/usr/bin/env bash

out_path=clients.txt
map_url="${PLATFORM_GET_MAPPING_URL:-"https://rc3jxwm5t6.execute-api.us-east-1.amazonaws.com/dev/getMapping"}"
jsonnet_url="${COMMERCE_DEPLOY_JOB_JSONNET_URL:-"https://gitlab.com/fabric2-public/cicd/cicd-templates/-/raw/main/.gitlab-ci/commerce_deploy_job.jsonnet"}"
stage="$PARENT_STAGE"
stage_suffix=""

if [[ "$stage" == *"release" ]]; then
	stage_suffix="release"
fi

curl \
	--header "x-api-key:${PLATFORM_API_KEY}" \
	--silent "$map_url" |
	jq \
		--arg environment "$ENV_NAME" \
		--raw-output \
		'.Items[] | select(.status == "active" and .env_type == "commerce" and .environment == $environment)' >"${out_path}"

# one environment (e.g. sandbox copilot) can have multiple stages.
# e.g. clientA uses dev01 for sandbox and clientB uses uat01
for stage in $STAGES; do
	clients=${clients}${s-}$(jq --arg stage "$stage" 'select(.stages[$stage] != null)|"\(.client):\($stage)"' "$out_path" | jq --raw-output --slurp 'join(" ")')
	s=" "
done

curl -O "$jsonnet_url"

# $clients will be in the format "clienta:uat clientb:uat clientc:dev01"
jsonnet -S \
	--ext-str client_list="$clients" \
	--ext-str stage_suffix="$stage_suffix" \
	commerce_deploy_job.jsonnet >generated-config.yml
