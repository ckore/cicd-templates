#!/usr/bin/env bash

set -eux

docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
aws ecr get-login-password --region ${AWS_DEFAULT_REGION} | docker login --username AWS --password-stdin $DOCKER_REGISTRY
docker pull ${IMAGE}
docker tag ${IMAGE} ${ECR_IMAGE}
docker push ${ECR_IMAGE}
