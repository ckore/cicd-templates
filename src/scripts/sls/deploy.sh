#!/usr/bin/env bash
set -eux

package_name="${PACKAGE_NAME:-$CI_PROJECT_NAME}"
cd "${BUILD_DIR}"

GITLAB_REGISTRY_ARTIFACT_URL="${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/${package_name}"
echo "GITLAB_REGISTRY_ARTIFACT_URL=$GITLAB_REGISTRY_ARTIFACT_URL"
echo "Preparing environment for ${CLIENT}"
secret_name=${CLIENT}/${STAGE}/secrets
param_name=${STAGE}_${PLATFORM}
_aws="aws"
aws configure set aws_access_key_id "${AWS_ACCESS_KEY_ID}"
aws configure set secret_access_key "${AWS_SECRET_ACCESS_KEY}"
aws configure set default.region us-east-1
if [[ ${CLIENT} != "greatwall" ]]; then
	stat client-accounts-map.json &>/dev/null || aws secretsmanager get-secret-value --secret-id bitbucket/awsaccess/clients | jq '.SecretString | fromjson' >client-accounts-map.json
	account_id=$(jq -r --arg client "${CLIENT}" '.[$client]' client-accounts-map.json)
	aws configure set profile.deploy.role_arn arn:aws:iam::"${account_id}":role/OrganizationAccountAccessRole
	aws configure set profile.deploy.source_profile default
	aws configure set default.aws_access_key_id "${AWS_ACCESS_KEY_ID}"
	aws configure set default.aws_secret_access_key "${AWS_SECRET_ACCESS_KEY}"
	export AWS_PROFILE=deploy
	export AWS_SDK_LOAD_CONFIG="1"
	_aws="aws --profile deploy"
fi
${_aws} secretsmanager get-secret-value --secret-id "${secret_name}" | jq -r '.SecretString | fromjson' >secret.env.json
${_aws} ssm get-parameter --name "${param_name}" | jq -r '.Parameter.Value | fromjson' >params.env.json
echo >.env
for json in params.env.json secret.env.json; do
	jq -r 'to_entries|map("export \(.key)=\"\(.value|tostring)\"")|.[]' "${json}" >>.env
done
echo "Downloading build artifact"
http_code=$(curl -LO -H "JOB-TOKEN:${CI_JOB_TOKEN}" -o/dev/null -w "%{http_code}" "${GITLAB_REGISTRY_ARTIFACT_URL}"/"${STAGE}"/"${BUILD_ID}".zip)
echo "${http_code}"
if [ "${http_code}" -ne 200 ]; then
	echo "/${STAGE}/${BUILD_ID}.zip not found; copying artifact to stage ${STAGE}"
	curl \
		-LO \
		-H "JOB-TOKEN:${CI_JOB_TOKEN}" \
		-o/dev/null \
		-w "%{http_code}" \
		"${GITLAB_REGISTRY_ARTIFACT_URL}/build/${BUILD_ID}".zip
fi
echo //registry.npmjs.org/:_authToken="${NPM_TOKEN}" >~/.npmrc
if [ ! -d node_modules ]; then npm install; fi
unzip -o "${BUILD_ID}.zip"
tar --overwrite -xzf build.tar.gz
source .env
# `sls package` only works with a ".webpack" directory so rename it here
mv build .webpack
apiName="${API_NAME:-"empty"}"
functionSet="${FUNCTION_SET:-"empty"}"
options="--no-build --stage ${STAGE} --client ${CLIENT} --platform ${PLATFORM} --api_host ${SLS_API_HOST} --api_cert ${SLS_API_CERT} --apiName ${apiName} --functionSet ${functionSet} --verbose"
sls package $options
sls deploy $options

if [ "${http_code}" -ne 200 ]; then
	curl \
		-H "JOB-TOKEN:${CI_JOB_TOKEN}" \
		-X PUT \
		--silent \
		--data-binary "@${BUILD_ID}.zip" \
		--write-out "%{http_code}" \
		--output /dev/null "${GITLAB_REGISTRY_ARTIFACT_URL}"/"${STAGE}"/"${BUILD_ID}".zip
fi
