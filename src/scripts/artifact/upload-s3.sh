#!/usr/bin/env bash

set -eux

# Keeping artifact_s3_path for backwards compatibility
# Alias artifact_s3_path because it's a misleading variable name
local_path=${local_path:-$artifact_s3_path}

if [[ -z ${artifact_bucket-} ]]; then
	echo "ERROR: missing required artifact_bucket"
	exit 1
fi
if [[ -z ${local_path-} ]]; then
	echo "ERROR: missing required local_path"
	exit 1
fi
artifact_s3_url=s3://$artifact_bucket/$CI_PROJECT_NAME/$BUILD_ID.zip
if aws s3 ls "$artifact_s3_url"; then
	echo "Artifact $artifact_s3_url already exists."
	exit 0
fi
env=${build_environment:-$CI_ENVIRONMENT_NAME}
mkdir artifact-contents
cp -r "$local_path"/* artifact-contents
tar -czvf "$env.tar.gz" artifact-contents
zip -r "${BUILD_ID}.zip" "$env.tar.gz"
aws s3 cp "${BUILD_ID}.zip" "$artifact_s3_url"
rm -rf "${BUILD_ID}.zip"
rm -rf artifact-contents
