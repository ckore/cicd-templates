#!/usr/bin/env bash

set -euxo pipefail

# DIST_DIR <required>
# BUILD_DIR | ./
# BUILD_CMD | <required>
# ARTIFACT_NAME | COMMIT_SHORT_SHA.zip

build_env="build"
build_dir=${BUILD_DIR:-"./"}
envs="$DEV_ENV $QA_ENV $SANDBOX_ENV $PROD_ENV"
echo "dist: $DIST_DIR"

function short_circuit_if_build_exists() {
	url="${GITLAB_REGISTRY_ARTIFACT_URL}/$build_env/$name"
	http_code=$(curl -H "$header_auth" -w "%{http_code}" -o/dev/null -sLO "$url")
	if [ "$http_code" -eq 200 ]; then
		echo "Artifact already exists"
		exit 0
	fi
}

short_circuit_if_build_exists

artifact_bundle=""
for env in $envs; do
	cd "${CI_PROJECT_DIR}/${build_dir}"
	NODE_ENV=$env $BUILD_CMD
	mkdir artifact-contents/
	cp -r ${CI_PROJECT_DIR}/${DIST_DIR}/* artifact-contents
	tar -czvf "$env.tar.gz" artifact-contents
	rm -rf artifact-contents
	artifact_bundle="$artifact_bundle ${env}.tar.gz"
done

zip -r "$name" ${artifact_bundle}
upload_artifact "$name" "$build_env"
