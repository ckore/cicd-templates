local boilerplate = importstr "../lib/boilerplate.txt";
local pipeline = {
  // This file owns the review deployment lifecycle end-to-end.
  // terraform, test, build, deploy, and teardown are bundled here.
  // Triggered by pushes to a merge request branch
  include: [
    {
      'remote': 'https://fabric-ci-templates.s3.amazonaws.com/release/v0.0.1/sls.yml',
    },
    {
      'remote': 'https://fabric-ci-templates.s3.amazonaws.com/release/v0.0.1/postman.yml',
    },
    {
      'remote': 'https://fabric-ci-templates.s3.amazonaws.com/release/v0.0.1/codequality.yml',
    },
    {
      'remote': 'https://fabric-ci-templates.s3.amazonaws.com/release/v0.0.1/a11y.yml',
    },
    {
      'remote': 'https://fabric-ci-templates.s3.amazonaws.com/release/v0.0.1/npm.yml',
    },
    {
      'remote': 'https://fabric-ci-templates.s3.amazonaws.com/release/v0.0.1/artifacts.yml',
    },
    {
      'remote': 'https://fabric-ci-templates.s3.amazonaws.com/release/v0.0.1/ping.yml',
    },
    {
      'remote': 'https://fabric-ci-templates.s3.amazonaws.com/release/v0.0.1/sast.yml',
    },
    {
      'remote': 'https://fabric-ci-templates.s3.amazonaws.com/release/v0.0.1/secret-detection.yml',
    },
    {
      'remote': 'https://fabric-ci-templates.s3.amazonaws.com/release/v0.0.1/browser-performance.yml',  
    },
  ],
  image: 'registry.gitlab.com/fabric2/ci-templates/node:10.15.3-ee00170d',
  variables: {
    BUILD_ID: '${CI_COMMIT_SHORT_SHA}',
    GITLAB_REGISTRY_ARTIFACT_URL: '${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/ci-builds',
    // TODO: document how to get postman collection/environment UUID
    NPM_LINT_CMD: "",  
    NPM_UNIT_TEST_CMD: "",  
    POSTMAN_COLLECTION_UUID: "",
    DEV_ENVIRONMENT_UUID: "",
    UAT_ENVIRONMENT_UUID: "",
    PROD_ENVIRONMENT_UUID: "",
    DEV_STAGE_NAME: "",
    UAT_STAGE_NAME: "",
    PROD_STAGE_NAME: ""
  },
  stages: [
    'build',
    'hotfix',
    'commerce',
    'test',
    'deploy-uat',
    'test-uat',
    'deploy-prod',
    'test-prod',
  ],
  '.default-run-policy': {
    only: {
      refs: [
        'main',
        'gitlab-ci-build',
        'test-pipeline-template'
      ],
      variables: [
        '$CI_COMMIT_MESSAGE !~ /.*\\[hotfix\\].*/',
      ],
    },
    needs: [],
  },
  build: {
    stage: 'build',
    extends: [
      '.sls-webpack',
      '.default-run-policy',
    ],
    variables: {
      // TODO: remove STAGE from build step
      // STAGE: uat
      SLS_DEBUG: '*',
    },
    needs: [],
  },
  hotfix: {
    stage: 'hotfix',
    variables: {
      PARENT_PIPELINE_ID: '$CI_PIPELINE_ID',
    },
    trigger: {
      include: [
        {
          'remote': 'https://fabric-ci-templates.s3.amazonaws.com/release/v0.0.1/pipelines/serverless-hotfix.yml',
        },
      ],
      strategy: 'depend',
    },
    only: {
      variables: [
        '$CI_COMMIT_MESSAGE =~ /.*\\[hotfix\\].*/',
      ],
    },
    needs: [
      'build',  
    ],
  },
  lint: {
    extends: [
      '.npm-run-10',
      '.default-run-policy',
    ],
    stage: 'test',
    variables: {
      npm_cmd: '$NPM_LINT_CMD',
    },
    needs: ['build'],
    // only: {
    //   variables: ['$NPM_LINT_CMD != ""']
    // },
  },
  test: {
    stage: 'test',
    extends: [
      '.npm-run',
      '.default-run-policy',
    ],
    variables: {
      npm_cmd: '$NPM_UNIT_TEST_CMD',
    },
    needs: ['build'],
    // only: {
    //   variables: ['"$NPM_UNIT_TEST_CMD" != ""']
    // },
  },
  'code-quality': {
    extends: [
      '.code-quality',
      '.default-run-policy',
    ],
    needs: [
      'build', 
    ],
    // only: {
    //   variables: [
    //     '"$DISABLE_CODE_QUALITY" == ""'
    //   ],
    // },
  },
  'secret-scanner': {
    extends: [
      '.secret_detection_default_branch',
      '.default-run-policy',
    ],
    needs: [
      'build', 
    ],
    // only: {
    //   variables: [
    //     '"$DISABLE_SECRET_SCANNER" == ""'
    //   ],
    // },
  },
  'semgrep-sast': {
    cache: {
    },
    extends: [
      '.semgrep-sast',
      '.default-run-policy',
    ],
    dependencies: [
    ],
    needs: [
      'build', 
    ],
    // only: {
    //   variables: [
    //     '"$DISABLE_SAST" == ""'
    //   ],
    // },
  },
  'gen-commerce': {
    image: 'alpine:latest',
    stage: 'commerce',
    script: |||
      apk update && apk add jsonnet
      jsonnet \
        --ext-str client_list="$CLIENT_LIST" \
        --ext-str stage="$STAGE" \
        --string .gitlab-ci/commerce-deploys.jsonnet  > generated-config.yml
    |||,
    artifacts: {
      paths: [
        'generated-config.yml',
      ],
    },
    needs: [
      'build', 
    ],
  },
  'copilot-uat-deploy': {
    stage: 'deploy-uat',
    extends: [
      '.default-run-policy',
      '.sls-deploy',
    ],
    environment: {
      name: '$UAT_STAGE_NAME',
    },
    variables: {
      CLIENT: 'greatwall',
      PLATFORM: 'copilot',
      STAGE: '$UAT_STAGE_NAME',
    },
    dependencies: [
      'build',
    ],
    needs: [
      'gen-commerce',
      'build',
      'test',
      'lint',
      'code-quality',
      'semgrep-sast',
      'secret-scanner', 
    ],
  },
  'commerce-uat-deploy': {
    stage: 'deploy-uat',
    trigger: {
      include: [
        {
          artifact: 'generated-config.yml',
          job: 'gen-commerce',
        },
      ],
      strategy: 'depend',
    },
    variables: {
      STAGE: '$UAT_STAGE_NAME',
    },
    needs: [
      'gen-commerce',
      'build',
      'test',
      'lint',
      'code-quality',
      'semgrep-sast',
      'secret-scanner',
    ],
  },
  'postman-uat': {
    extends: [
      '.postman',
      '.default-run-policy',
    ],
    stage: 'test-uat',
    variables: {
      POSTMAN_ENVIRONMENT_UUID: '$POSTMAN_UAT_ENVIRONMENT_UUID',
    },
    needs: [
      'copilot-uat-deploy',  
    ],
    // only: {
    //   variables: [
    //     '"$DISABLE_POSTMAN" == ""'
    //   ],
    // },
  },
  'commerce-prod-deploy': {
    stage: 'deploy-prod',
    needs: [
      'commerce-uat-deploy',
    ],
    trigger: {
      include: [
        {
          artifact: 'generated-config.yml',
          job: 'gen-commerce',
        },
      ],
      strategy: 'depend',
    },
    variables: {
      STAGE: '$PROD_STAGE_NAME',  
    },
  },
  'copilot-prod-deploy': {
    stage: 'deploy-prod',
    extends: [
      '.sls-deploy',
      '.default-run-policy',
    ],
    cache: {
    },
    variables: {
      CLIENT: 'greatwall-production',
      PLATFORM: 'copilot',
      STAGE: '$PROD_STAGE_NAME',
      PREVIOUS_STAGE: '$UAT_STAGE_NAME',
    },
    environment: {
      name: '$PROD_STAGE_NAME',
    },
    dependencies: [
      'build',
    ],
    needs: [
      'build',
      'copilot-uat-deploy',
      'postman-uat',  
    ],
  },
  'postman-prod': {
    extends: [
      '.postman',
      '.default-run-policy',
    ],
    stage: 'test-prod',
    variables: {
      POSTMAN_ENVIRONMENT_UUID: '$POSTMAN_PROD_ENVIRONMENT_UUID',
    },
    needs: [
      'copilot-prod-deploy',  
    ],
    // only: {
    //   variables: [
    //     '"$DISABLE_POSTMAN" == ""'
    //   ],
    // },
  },
};
boilerplate + std.manifestYamlDoc(pipeline, indent_array_in_object=true)
