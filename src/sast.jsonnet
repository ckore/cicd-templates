local boilerplate = importstr "lib/boilerplate.txt";
# https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Jobs/SAST.gitlab-ci.yml[]
# Read more about this feature here: https://docs.gitlab.com/ee/user/application_security/sast/
#
# Configure SAST with CI/CD variables (https://docs.gitlab.com/ee/ci/variables/README.html).
# List of available variables: https://docs.gitlab.com/ee/user/application_security/sast/index.html#available-variables
local variables = {
  variables: {
    # Setting this variable will affect all Security templates
    # (SAST, Dependency Scanning, ...)
    SECURE_ANALYZERS_PREFIX: "registry.gitlab.com/gitlab-org/security-products/analyzers",
    SAST_DEFAULT_ANALYZERS: "bandit, brakeman, gosec, spotbugs, flawfinder, phpcs-security-audit, security-code-scan, nodejs-scan, eslint, sobelow, pmd-apex, kubesec, mobsf, semgrep",
    SAST_EXCLUDED_ANALYZERS: "",
    SAST_EXCLUDED_PATHS: "spec, test, tests, tmp",
    SAST_ANALYZER_IMAGE_TAG: 2,
    SCAN_KUBERNETES_MANIFESTS: "false"
  }
};
local job_templates = {
  ".sast": {
    stage: "test",
    variables: {
      SEARCH_MAX_DEPTH: 4
    },
    artifacts: {
      reports: {
        sast: "gl-sast-report.json"
      },
    },
  },
  ".sast-analyzer": {
    extends: ".sast",
    allow_failure: true,
    script: "/analyzer run"
  },
  ".eslint-sast": {
    extends: ".sast-analyzer",
    image: "$SAST_ANALYZER_IMAGE",
    variables: {
          # SAST_ANALYZER_IMAGE is an undocumented variable used internally to allow QA to
      # override the analyzer image with a custom value. This may be subject to change or
      # breakage across GitLab releases.
      SAST_ANALYZER_IMAGE: "$SECURE_ANALYZERS_PREFIX/eslint:$SAST_ANALYZER_IMAGE_TAG"
    }
  },
  ".nodejs-scan-sast": {
    extends: ".sast-analyzer",
    image: "$SAST_ANALYZER_IMAGE",
    variables: {
      # SAST_ANALYZER_IMAGE is an undocumented variable used internally to allow QA to
      # override the analyzer image with a custom value. This may be subject to change or
      # breakage across GitLab releases.
      SAST_ANALYZER_IMAGE: "$SECURE_ANALYZERS_PREFIX/nodejs-scan:$SAST_ANALYZER_IMAGE_TAG"
    }
  },
  ".semgrep-sast": {
    extends: ".sast-analyzer",
    image: "$SAST_ANALYZER_IMAGE",
    variables: {
      # SAST_ANALYZER_IMAGE is an undocumented variable used internally to allow QA to
      # override the analyzer image with a custom value. This may be subject to change or
      # breakage across GitLab releases.
      SAST_ANALYZER_IMAGE: "$SECURE_ANALYZERS_PREFIX/semgrep:$SAST_ANALYZER_IMAGE_TAG"
    }
  }
};
local config = variables + job_templates;
boilerplate + std.manifestYamlDoc(config, indent_array_in_object=true)
