local boilerplate = importstr "lib/boilerplate.txt";
local dind = import "lib/dind.libsonnet";

local global_vars = {
  variables: {
    IMAGE: "$CI_REGISTRY_IMAGE:latest",
    DEPLOYMENT_ROLE: "arn:aws:iam::${ACCOUNT_NUMBER}:role/OrganizationAccountAccessRole"
  },
};
local job_templates = {
  ".deployment_script": {
    image: {
      name: "registry.gitlab.com/rafidsaad/images/aws-docker:latest",
      entrypoint: [""],
    },
    before_script: importstr "scripts/docker/deployment.sh"
  },
  ".build_and_push_gitlab": dind.DindJob {
    script: importstr "scripts/docker/build-and-push.sh"
  }
};

local gitlabCiConf =  global_vars +
                      job_templates;

boilerplate + std.manifestYamlDoc(gitlabCiConf, indent_array_in_object=true)
