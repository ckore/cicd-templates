# ci-templates

## Dependencies
1. Jsonnet:
Run `brew install jsonnet`
2. shellcheck:
Run `brew install shellcheck`
## Build

CICD templates are compiled via Jsonnet. Once the dependencies are installed you can run the Makefile to build the templates. Simply run `make`.

## Templates

The templates can serve one of five high-level functions as outlined below: build, quality gates, security gates, deploy, terraform, k8s.

### Build templates

These templates are responsible for building and storing artifacts. The `.artifact-*` templates are used for static S3 site projects, and the `.sls-*` templates are for serverless projects.

#### .sls-webpack

Runs `npm install` and `sls webpack` to create the serverless build artifacts, which get stored in the project's private artifact registry on Gitlab.

| Variable Name | Description | Required | Default |
| ---- | ---- | ---- | ---- |
| BUILD_DIR | Build directory | no | ./ |
| PLATFORM | The platform (`commerce` or `copilot`) | yes | |
| PREVIOUS_STAGE | The previous stage the service was deployed to in the pipeline (for build promotion purposes) | yes | |
| STAGE | The target stage to deploy to | yes | |
| CLIENT | The client name | yes | |
| PLATFORM_DISABLE_HISTORY | If set to a truthy value, the job will not report to the platform API. | no | "" |

#### .build-and-push-gitlab

Builds a Docker image and pushes it to the project's container registry.

| Variable Name | Description | Required | Default |
| ---- | ---- | ---- | ---- |
| IMAGE| The image name and tag | yes | |

#### .npm-run

Runs arbitrary npm command. Can also be used as a means to run unit tests.

| Variable Name | Description | Required | Default |
| ---- | ---- | ---- | ---- |
| BUILD_DIR | Build directory | no | ./ |
| npm_cmd | The npm command to run | yes | |
| report_dir | Directory to look for reports | no | ./ |
| npm_dir | Directory in which to run npm command | no | ./ |

#### .artifact-upload-to-s3

Compresses and uploads the build output for React builds to an s3 bucket.

| Variable Name | Description | Required | Default |
| ---- | ---- | ---- | ---- |
| artifact_s3_path | Local path of the artifact directory | yes | |
| artifact_bucket | The s3 bucket that the artifact will be uploaded to | yes | |
| build_environment | The name of the build environment (e.g. dev, prod) | no | `$CI_ENVIRONMENT_NAME` |

#### .artifact-promote

Copies a build artifact from one bucket to another. Used when artifact buckets are siloed by stage.

| Variable Name | Description | Required | Default |
| ---- | ---- | ---- | ---- |
| source_env_bucket | The source bucket name | yes | |
| target_env_bucket | The target bucket name| yes | |

### Quality gates

Unit testing, API testing, quality gates, etc.

#### .jmeter

Runs [jmeter](https://jmeter.apache.org/) performance test.

| Variable Name | Description | Required | Default |
| ---- | ---- | ---- | ---- |
| JMX_PATH | The file path to the jmx file | yes | |

#### .a11y

Runs [a11y](https://www.a11yproject.com/) accessibility report on a URL.

| Variable Name | Description | Required | Default |
| ---- | ---- | ---- | ---- |
| a11y_url | The URL to test | yes | |

#### .browser-performance

Runs a [sitespeed](https://www.sitespeed.io/) browser performance report on a given URL.

| Variable Name | Description | Required | Default |
| ---- | ---- | ---- | ---- |
| URL | The URL to test | yes | |

#### .code-quality

Runs [codeclimate](https://docs.gitlab.com/ee/user/project/merge_requests/code_quality.html) code quality report.

| Variable Name | Description | Required | Default |
| ---- | ---- | ---- | ---- |

#### .ping

Runs a curl command to test for a http status code. The job fails if the status code is unexpected.

| Variable Name | Description | Required | Default |
| ---- | ---- | ---- | ---- |
| PING_URL | The URL to ping | yes | |
| PING_STATUS_CODE_SUCCESS | The desired http status code | no | 200 |
| PING_TRIES | The number of failed attempts .ping will make before exiting. | no | 5 |
| PING_SLEEP_SECONDS | The number of seconds to wait betwen tries | no | 5 |

#### .postman

Runs a postman collection.

| Variable Name | Description | Required | Default |
| ---- | ---- | ---- | ---- |
| POSTMAN_ENVIRONMENT_UUID | The environment UUID; can be obtained from the [Postman API](https://community.postman.com/t/where-do-you-see-your-own-uid-for-collection/3537) | yes | |
| POSTMAN_COLLECTION_UUID | The collection UUID; can be obtained from the [Postman API](https://community.postman.com/t/where-do-you-see-your-own-uid-for-collection/3537) | yes | |

### Security gates

#### .semgrep-sast

Runs [semgrep](https://semgrep.dev/) static analysis report. Report artifact is exported to Gitlab.

| Variable Name | Description | Required | Default |
| ---- | ---- | ---- | ---- |

#### .secret_detection_default_branch

Runs [gitleaks](https://github.com/zricethezav/gitleaks) secret detection report. Report artifact is exported to Gitlab. Used specifically for jobs running on `$CI_DEFAULT_BRANCH`.

| Variable Name | Description | Required | Default |
| ---- | ---- | ---- | ---- |

#### .secret_detection

Runs [gitleaks](https://github.com/zricethezav/gitleaks) secret detection report. Use this template for any branch other than `$CI_DEFAULT_BRANCH` (for comparison purposes these have to be split into distinct jobs). Report artifact is exported to Gitlab.

| Variable Name | Description | Required | Default |
| ---- | ---- | ---- | ---- |

### Deploy templates

#### .sls-deploy

Fetches secrets and parameter store environment variables from the client's AWS account, generates Cloudformation templates to deploy along with the build artifacts generated by `.sls-webpack`. By default it POSTs to the `platform.fabric.zone` REST APIs to create and update job status.

| Variable Name | Description | Required | Default |
| ---- | ---- | ---- | ---- |
| PLATFORM | The platform (`commerce` or `copilot`) | yes | |
| PREVIOUS_STAGE | The previous stage the service was deployed to in the pipeline (for build promotion purposes) | yes | |
| STAGE | The target stage to deploy to | yes | |
| CLIENT | The client name | yes | |
| PLATFORM_DISABLE_HISTORY | If set to a truthy value, the job will not report to `platform.fabric.zone` | no | "" |

### .artifact-deploy-to-s3

Pulls an artifact down from s3, unzips it, and syncs the contents with an s3 site bucket.

| Variable Name | Description | Required | Default |
| ---- | ---- | ---- | ---- |
| target_env_bucket | The artifact bucket name | yes | |
| deploy_s3_bucket | The website bucket name | no | `$CI_ENVIRONMENT_NAME`
| build_environment | The environment name (dev, prod) | no | `$CI_ENVIRONMENT_NAME` | |

### .artifact-promote-and-deploy-to-s3

Combines the previous two templates. The variables are a union of the two.

### k8s

### .helm

alpine/helm:3.6.2 base image for helm jobs. Pre-installs aws_auth plugin for eks cluster authentication.

| Variable Name | Description | Required | Default |
| ---- | ---- | ---- | ---- |
| KUBECONFIG | Path to the kubeconfig file | yes | |

### .kube

bitnami/kubectl:1.20.9 base image for kubectl jobs. Pre-installs aws_auth plugin for eks cluster authentication.

| Variable Name | Description | Required | Default |
| ---- | ---- | ---- | ---- |
| KUBECONFIG | Path to the kubeconfig file | yes | |

### Terraform templates

#### .terraform-validate

Runs `terraform fmt`, and `terraform validate`.

| Variable Name | Description | Required | Default |
| ---- | ---- | ---- | ---- |
| tf_dir | The directory in which to run the terraform command | yes | |

#### .terraform-init

Runs `terraform init`.

| Variable Name | Description | Required | Default |
| ---- | ---- | ---- | ---- |
| tf_dir | The directory in which to run the terraform command | yes | |

#### .terraform-plan

Runs `terraform plan` and saves the output as a report artifact, which is viewable in Gitlab merge requests.

| Variable Name | Description | Required | Default |
| ---- | ---- | ---- | ---- |
| tf_dir | The directory in which to run the terraform command | yes | |
| tf_workspace_name | The name of the terraform workspace | no | `$CI_ENVIRONMENT_NAME` |
| tf_vars_file_name | The name of the terraform vars file to use | no | `$CI_ENVIRONMENT_NAME` |

#### .terraform-apply

Runs `terraform apply` on the plan created perviously.

| Variable Name | Description | Required | Default |
| ---- | ---- | ---- | ---- |
| tf_dir | The directory in which to run the terraform command | yes | |
| tf_workspace_name | The name of the terraform workspace | no | `$CI_ENVIRONMENT_NAME` |
| tf_vars_file_name | The name of the terraform vars file to use | no | `$CI_ENVIRONMENT_NAME` |

### .terraform-destroy

Runs `terraform destroy` and deletes the workspace associated with the environment.

| Variable Name | Description | Required | Default |
| ---- | ---- | ---- | ---- |
| tf_dir | The directory in which to run the terraform command | yes | |
| tf_vars_file_name | The vars file to use | no | `$CI_ENVIRONMENT_NAME` |
| tf_workspace_name | The workspace to remove after `terraform destroy` | no | `$CI_ENVIRONMENT_NAME` |

## Example
